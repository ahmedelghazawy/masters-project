#!/bin/bash
for (( i = 0; i < 10; i++ )); do
    python3 01_generate_ifg_v2.py
    mkdir batch_$i
    mv out/* batch_$i/
    cp batch_$i/Interferogram0.jpg out/Interferogram0.jpg
done
