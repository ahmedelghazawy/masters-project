import os
from tqdm import tqdm
import cv2
import matplotlib.pyplot as plt
import pickle
import numpy as np

class ToPickles:
    def __init__(self):
        self.IMG_DIR = "images3"
        self.SIZE = 32
        self.IMG_CHANNEL = 3
        self.training_data = []

        #used to get the path to the images
        self.BATCHES = [f"batch_{str(i)}" for i in range(0, 50)]

    def store_all_to_array(self):
        #for all batches
        counter = 1
        try:
            for batch in self.BATCHES:
                path = os.path.join(self.IMG_DIR, batch)
                for i in range(0, 1000):
                    image_name = f"{str(i)}.jpg"
                    path_to_single_img = os.path.join(path, image_name)

                    img_array = cv2.imread(path_to_single_img)
                    new_img_array = cv2.resize(img_array, (self.SIZE, self.SIZE))
                    new_img_array = new_img_array[:, :, [2, 1, 0]]
                    self.training_data.append(new_img_array)

                    print(f"Working on batch : {batch}, image : {counter}")
                    counter += 1


        except Exception as e:
            print(e)

    def save_to_pickles(self):
        first_input_array = self.training_data[0:-2]
        second_input_array = self.training_data[1:-1]
        labels = self.training_data[2:]

        X1 = np.array(first_input_array).reshape(-1, self.SIZE, self.SIZE, 3).astype('float32') / 255.0
        # X2 = np.array(second_input_array).reshape(-1, self.SIZE, self.SIZE, 3) / 255.0
        # y = np.array(labels).reshape(-1, self.SIZE, self.SIZE, 3) / 255.0
        #
        pickle_out = open("new_data_X_50.pickle", "wb")
        pickle.dump(X1, pickle_out)
        pickle_out.close()
        #
        # pickle_out = open("32b32X2.pickle", "wb")
        # pickle.dump(X2, pickle_out)
        # pickle_out.close()

        # pickle_out = open("32b32y.pickle", "wb")
        # pickle.dump(y, pickle_out)
        # pickle_out.close()

        print("Done Saving")





to_pickle = ToPickles()
to_pickle.store_all_to_array()
to_pickle.save_to_pickles()



